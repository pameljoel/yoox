function formValidation() {
    var name = $('#contact-name');
    var email = $('#contact-email');
    var phone = $('#contact-phone');
    var submit = $('#contact-submit button');
    var emailValidation = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    var phoneValidation = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;

    // name is required
    name.on('input', function () {
        var input = $(this);
        var is_name = input.val();
        if (is_name) {
            input.removeClass("invalid").addClass("valid");
        }
        else {
            input.removeClass("valid").addClass("invalid");
        }
    });

    // Email must be an email
    email.on('input', function () {
        var input = $(this);
        var re = emailValidation;
        var is_email = re.test(input.val());
        if (is_email) {
            input.removeClass("invalid").addClass("valid");
        }
        else {
            input.removeClass("valid").addClass("invalid");
        }
    });

    // Phone must be a number
    phone.on('input', function () {
        var input = $(this);
        var re = phoneValidation;
        var is_phone = re.test(input.val());
        if (is_phone) {
            input.removeClass("invalid").addClass("valid");
        }
        else {
            input.removeClass("valid").addClass("invalid");
        }
    });

    // After Form Submitted Validation
    submit.click(function (event) {
        event.preventDefault();
        var form_data = $("#contact").serializeArray();
        var error_free = true;

        for (var input in form_data) {
            var element = $("#contact-" + form_data[input]['name']);
            var valid = element.hasClass("valid");
            var error_element = $("span", element.parent());
            if (!valid) {
                error_element.removeClass("error").addClass("error-show");
                error_free = false;
            }
            else {
                error_element.removeClass("error-show").addClass("error");
            }
        }

        if (!error_free) {
            console.log('Error, please fill in the details');
        }
        else {
            console.log('No errors: Form will be submitted');
        }
    });

}
