function getNews() {

    var i = 1;
    var collapsedItemParent = '.news-block';
    var collapsedItem = collapsedItemParent + ' > .collapse';
    var collapseInItem = '.collapse-in';
    var loadMore = $('#load-more-news');
    var loadLess = $('#load-less-news');

    function collapseLength() {
        i = $(collapsedItem).size();
        return i;
    }

    function renderButton() {
        if (collapseLength() > 0) {
            showMoreButton();
        } else {
            showLessButton();
        }
    }

    function showMoreButton() {
        loadMore.fadeIn();
        loadLess.fadeOut();
    }

    function showLessButton() {
        loadMore.fadeOut();
        loadLess.fadeIn();
    }

    loadLess.click(function () {
        $(collapseInItem)
            .removeClass('collapse-in')
            .addClass('collapse');
        renderButton();
    });

    loadMore.click(function () {
        $(collapsedItem)
            .first()
            .addClass('collapse-in')
            .removeClass('collapse');
        renderButton();
    });

    renderButton();

}