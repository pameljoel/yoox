function carouselSlider() {
    $('.carousel-container').flexslider({
        animation: "slide",
        namespace: "carousel-",
        startAt: 1,
        animationLoop: true,
        keyboard: true
    });
}
