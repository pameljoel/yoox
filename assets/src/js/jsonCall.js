function showcaseJsonCall() {

    var item = $('.showcase-selection-item');

    item.click(function () {

        if (!$(this).hasClass('active')) {

            var id = $(this).attr('id');
            $(item).removeClass('active');
            $(this).addClass('active');
            jsonDataCall(id);

        }
    });

    function jsonDataCall(id) {
        var url = 'assets/data/' + id + '.json';
        getJson(url);
    }

    jsonDataCall('one');

}