function showcaseSlider() {

    $('.showcase-slides').flexslider({
        namespace: "showcase-",
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        sync: ".showcase-thumbs",
        directionNav: true
    });

    $('.showcase-thumbs').flexslider({
        namespace: "thumbs-",
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        asNavFor: '.showcase-slides',
        directionNav: false
    });

}