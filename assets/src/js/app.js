/* author: Pamel Joel Beltr� */

$(document).ready(function () {

    /* main carousel start */
    carouselSlider();

    /* get showcase content */
    showcaseJsonCall();

    /* showcase carousel start */
    showcaseSlider();

    /* call form validation */
    formValidation();

    /* show news */
    getNews();

});