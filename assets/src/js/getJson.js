function getJson(url) {

    // selectors
    var slides = $('.showcase-slides');
    var thumbs = $('.showcase-thumbs');
    var text = $('.showcase-text');

    //css classes selectors
    var thumbsChild = 'showcase-content-thumb';
    var lead = 'showcase-content-lead-image';

    $.getJSON(url)
        .done(function (data) {
            var item = data.item;

            /* reset carousel */
            clearCarousel();
            /* get descriptions */
            getTextCarousel(item);
            /* Get images */
            getImageCarousel(item);
            /* Get thumbs */
            getThumbsCarousel(item);
            /* restart carousel*/
            showcaseSlider(item);

        })
        .fail(function () {
            alert('error');
            console.log(data);
        });


    function clearCarousel() {
        slides.removeData("flexslider");
        thumbs.removeData("flexslider");
    }

    function getTextCarousel(item) {

        /* get descriptions */
        text.html('');
        text.append(
            '<h2>' + item.name + '</h2>' +
            '<p>' + item.composition + '</p>'
        );
        for (var i = 0; i < item.modelDetails.length; i++) {
            $(text).append('<p><small>' + item.modelDetails[i] + '</small></p>');
        }
    }

    // big image
    function getImageCarousel(item) {

        /* Get images */
        slides.html('');
        slides.append('<ul class="slides"></ul>');
        for (var i = 0; i < item.images.length; i++) {
            $('.showcase-slides .slides').append(
                '<li>' +
                '<img class="' + lead +
                '" src="' + item.images[i] +
                '" alt="' + item.details +
                '">' +
                '</li>'
            );
        }
    }

    // thumbnails
    function getThumbsCarousel(item) {

        /* Get thumbs */
        thumbs.html('');
        thumbs.append('<ul class="slides inline"></ul>');
        for (var i = 0; i < item.images.length; i++) {
            $('.showcase-thumbs .slides').append(
                '<li>' +
                '<img class="' + thumbsChild +
                '" src="' + item.images[i] +
                '">' +
                '</li>'
            );
        }
    }
}
