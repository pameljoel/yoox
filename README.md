`Author: Pamel Joel Beltr�`

# README #

This README documents the steps that are necessary to get the application up and running.

# This Project requires #

* GIT
* local server (wamp, xampp, mamp, etc)
* NPM
* NodeJS
* Ruby (for compass)
* grunt

# Project startup #

* npm install
* gem install compass
* npm install -g bower
* npm install -g grunt-cli
* bower install

# Project commands #

###dev 
* grunt
* grunt watch
* grunt clean

### production
* grunt compile

# project folder architecture #

### production assets: compressed and minified
* root/assets
* ./assets/css
* ./assets/data
* ./assets/fonts
* ./assets/images

### source files: uncompressed dev
* root/assets/src
* ./assets/src/js
* ./assets/src/scss

### source vendors: bower dependencies
* root/assets/src/js/vendor
* ./assets/src/js/vendor/flexslider
* ./assets/src/js/vendor/jquery

# Building process #

###git
* git init
* git remote add origin https://pameljoel@bitbucket.org/pameljoel/yoox
* echo "Pamel Joel Beltr�" >> contributors.txt
* git add contributors.txt
* about.md

###npm
* npm init

###grunt
* npm install -g grunt-cli
* npm install -g grunt-init
* npm install grunt --save-dev
* GruntFile.js

####grunt-watch
* npm install grunt-contrib-watch --save-dev

####grunt-uglify
* npm install grunt-contrib-uglify --save-dev

####grunt-jshint
* npm install grunt-contrib-jshint --save-dev

####grunt-compass
* npm install grunt-contrib-compass --save-dev

###bower
* npm instal bower --save-dev
* bower init

####jquery
* bower install jquery

## What is this repository for? ##

* Yoox front-end test page
* Version 1.00


## What does this repository use? ##

* node.js
* npm
* grunt
    * SASS
* compass
+ bower
    * flexslider
    * jquery

## Credits ##

Pamel Joel Beltr�
Graphic Designer & Front-End Developer
[www.pameljoel.it](http://www.pameljoel.it)