/**
 * www.pameljoel.it
 * @author Pamel Joel Beltrè
 */

'use strict';

/**
 * Grunt Module
 */
module.exports = function (grunt) {

    /**
     * Grunt Configuration
     */
    grunt.initConfig({

        /**
         * Get package.json
         */

        pkg: grunt.file.readJSON('package.json'),

        /**
         * Project links
         */

        project: {
            app: '../yoox',
            assets: '<%= project.app %>/assets',
            src: '<%= project.assets %>/src',
            scss: '<%= project.src%>/scss',
            css: '<%= project.assets%>/css',
            js: '<%= project.src %>/js',
            vendor: '<%= project.js %>/vendor',
            img: '<%= project.assets %>/images',
            font: '<%= project.assets %>/fonts',
            data: '<%= project.assets %>/data'
        },

        /**
         * banner
         */

        tag: {
            banner: ' * <%= pkg.name %>\n' +
            ' * <%= pkg.title %>\n' +
            ' * <%= pkg.url %>\n' +
            ' * @author <%= pkg.author %>\n' +
            ' * @version <%= pkg.version %>\n' +
            ' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n'
        },

        /**
         * uglify
         */

        uglify: {
            my_target: {
                files: {
                    'assets/js/app.js': [
                        '<%= project.vendor %>/jquery/dist/jquery.min.js',
                        '<%= project.vendor %>/flexslider/jquery.flexslider.js',
                        '<%= project.js %>/*.js'
                    ]
                }
            }
        },

        /**
         * concat
         */

        concat: {
            dist: {
                src: [
                    '<%= project.vendor %>/jquery/dist/jquery.min.js',
                    '<%= project.vendor %>/flexslider/jquery.flexslider.js',
                    '<%= project.js %>/*.js'
                ],
                dest: 'assets/js/app.js'
            }
        },


        /**
         * compass
         */

        compass: {
            dist: {
                options: {
                    sassDir: '<%= project.scss %>',
                    cssDir: '<%= project.css %>',
                    imagesDir: '<%= project.img %>',
                    fontsDir: '<%= project.font %>',
                    environment: 'production',
                    outputStyle: 'compressed',
                    noLineComments: true,
                }
            },
            dev: {
                options: {
                    sassDir: '<%= project.scss %>',
                    cssDir: '<%= project.css %>',
                    imagesDir: '<%= project.img %>',
                    fontsDir: '<%= project.font %>',
                    sourcemap: true,
                    environment: 'development'
                }
            },
            clean: {
                options: {
                    sassDir: '<%= project.scss %>',
                    cssDir: '<%= project.css %>',
                    environment: 'production',
                    clean: true
                }
            }
        },


        /**
         * Watch
         */

        watch: {
            css: {
                files: [
                    '<%= project.scss %>/*',
                    '<%= project.scss %>/*/*',
                    '<%= project.scss %>/*/*/*'
                ],
                tasks: 'compass:dev',
                options: {
                    spawn: false
                }
            },
            js: {
                files: [
                    '<%= project.js %>/*'
                ],
                tasks: 'concat'
            },
            livereload: {
                files: ['*.html', '*.php', 'images/**/*.{png,jpg,jpeg,gif,webp,svg}'],
                options: {
                    livereload: true
                }
            }
        }

    });

    /**
     * load grunt tasks
     */
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    /**
     * default grunt task
     */

    grunt.registerTask('default', [
        'compass:dev',
        'uglify'
    ]);

    grunt.registerTask('compile', [
        'compass:clean',
        'compass:dist',
        'uglify'
    ]);
};



